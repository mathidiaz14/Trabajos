@extends('layouts.app', ['active' => 'password'])

@section('content')
<div class="row">
    <div class="col-lg-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item" aria-current="page"><a href="{{url('home')}}">Dashboard</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{url('password')}}">Contraseñas</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar</li>
          </ol>
        </nav>
        <div class="card">
            <div class="header">
                <h4 class="title">Editar contraseña</h4>
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <form class="form-horizontal" action="{{url('password', $password->id)}}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Contraseña</label>
                                    <input type="text" class="form-control" value="{{$password->pass}}" name="pass">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <input type="text" class="form-control" value="{{$password->description}}" name="description">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Empresa</label>
                                    <select name="business_id" class="form-control">
                                    	@foreach(Auth::user()->businesses as $business)
											@if($business->id == $password->business_id)
												<option value="{{$business->id}}" selected="">{{$business->name}}</option>
											@else
												<option value="{{$business->id}}">{{$business->name}}</option>
											@endif
                                    	@endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-info btn-fill pull-right">
                                    <i class="pe-7s-diskette"></i>
                                    Guardar
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
