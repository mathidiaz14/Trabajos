@extends('layouts.app', ['active' => 'work'])

@section('css')
    <style>
        .table th:hover
        {
           cursor: pointer; 
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Trabajos</li>
          </ol>
        </nav>
        <div class="card">
            <div class="header">
                <div class="row">
                    <div class="col-xs-6">
                        <h4 class="title">Trabajos</h4>
                        <p class="category">Trabajos registradas</p>
                    </div>
                    <div class="col-xs-6 text-right">
                    	<a href="{{url('work/create')}}" class="btn btn-success btn-fill">
                            <i class="fa fa-plus"></i>
                            Agregar
                        </a>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="table table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                              <th scope="col">Fecha <small><i class="fa fa-sort"></i></small></th>
                              <th scope="col">Empresa <small><i class="fa fa-sort"></i></small></th>
                              <th scope="col">Descripción <small><i class="fa fa-sort"></i></small></th>
                              <th scope="col">Costo <small><i class="fa fa-sort"></i></small></th>
                              <th scope="col">Gano <small><i class="fa fa-sort"></i></small></th>
                              <th scope="col">Guardo <small><i class="fa fa-sort"></i></small></th>
                              <th scope="col">Estado <small><i class="fa fa-sort"></i></small></th>
                              <th scope="col">Vence <small><i class="fa fa-sort"></i></small></th>
                              <th>Editar</th>
                              <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(Auth::user()->works->sortByDesc('date') as $work)
                                <tr>
                                    <td>{{$work->date->format('d/m/Y')}}</td>
                                    <td>{{$work->business->name}}</td>
                                    <td>{{$work->description}}</td>
                                    <td>@if($work->cost == null) -- @else $ {{$work->cost}} @endif</td>
                                    <td>@if($work->gain == null) -- @else $ {{$work->gain}} @endif</td>
                                    <td>@if($work->saved == null) -- @else $ {{$work->saved}} @endif</td>
                                    <td>@if($work->status == 'pending') Pendiente @else Págo @endif</td>
                                    <td>@if($work->expires == null) -- @else {{$work->expires->format('d/m/Y')}} @endif</td>
                                    <td>
                                        <a href="{{route('work.edit', $work->id)}}" class="btn btn-primary btn-fill">
                                            <i class="fa fa-edit"></i>
                                            Editar
                                        </a>
                                    </td>
                                    <td>
                                        <form action="{{ url('work', $work->id)}}" method="POST">
                                            @csrf
                                            <input type='hidden' name='_method' value='DELETE'>
                                            <button class="btn btn-danger btn-fill">
                                                <i class="fa fa-trash"></i>
                                                Eliminar
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
       $(document).ready(function(){ 
            $("#table").tablesorter(); 
        }); 
    </script>
@endsection