@extends('layouts.app', ['active' => 'work'])

@section('content')
<div class="row">
    <div class="col-lg-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item" aria-current="page"><a href="{{url('home')}}">Dashboard</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{url('work')}}">Trabajos</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar</li>
          </ol>
        </nav>
        <div class="card">
            <div class="header">
                <h4 class="title">Editar trabajo</h4>
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <form class="form-horizontal" action="{{route('work.update', $work->id)}}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="PATCH">
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Fecha</label>
                                    <input type="date" class="form-control" name="date" value="{{$work->date->format('Y-m-d')}}">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Empresa</label>
                                    <select name="business_id" class="form-control">
                                    	@foreach(Auth::user()->businesses as $business)
											@if($business->id == $work->business_id)
												<option value="{{$business->id}}" selected>{{$business->name}}</option>
											@else
												<option value="{{$business->id}}">{{$business->name}}</option>
											@endif
                                    	@endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <input type="text" class="form-control" value="{{$work->description}}" name="description">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Estado</label>
                                    <select name="status" class="form-control">
                                        @if($work->status == "pending")
                                            <option value="pending" selected="">Pendiente</option>
                                            <option value="payment">Págo</option>
                                        @else
                                            <option value="pending">Pendiente</option>
                                            <option value="payment" selected="">Págo</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Costo</label>
                                    <input type="text" class="form-control" value="{{$work->cost}}" name="cost">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Gano</label>
                                    <input type="text" class="form-control" value="{{$work->gain}}" name="gain">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Guardo</label>
                                    <input type="text" class="form-control" value="{{$work->saved}}" name="saved">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Vence</label>
                                    <input type="date" class="form-control" value="{{$work->expires}}" name="expires">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-info btn-fill pull-right">
                                    <i class="pe-7s-diskette"></i>
                                    Guardar
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
