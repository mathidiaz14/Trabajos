@extends('layouts.app', ['active' => 'business'])

@section('content')
<div class="row">
    <div class="col-lg-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Trabajos</li>
          </ol>
        </nav>
        <div class="card">
            <div class="header">
                <div class="row">
                    <div class="col-xs-6">
                        <h4 class="title">Trabajos</h4>
                        <p class="category">Trabajos registradas por cliente</p>
                    </div>
                    <div class="col-xs-6 text-right">
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="table table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                              <th scope="col">Fecha</th>
                              <th scope="col">Descripción</th>
                              <th scope="col">Costo</th>
                              <th scope="col">Gano</th>
                              <th scope="col">Guardo</th>
                              <th scope="col">Vence</th>
                              <th scope="col">Editar</th>
                              <th scope="col">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($business->works as $work)
                                <tr>
                                    <td>{{$work->date}}</td>
                                    <td>{{$work->description}}</td>
                                    <td>$ {{$work->cost}}</td>
                                    <td>$ {{$work->gain}}</td>
                                    <td>$ {{$work->saved}}</td>
                                    <td>{{$work->expires}}</td>
                                    <td>
                                        <a href="{{route('work.edit', $work->id)}}" class="btn btn-primary btn-fill">
                                            <i class="fa fa-edit"></i>
                                            Editar
                                        </a>
                                    </td>
                                    <td>
                                        <form action="{{ url('work', $work->id)}}" method="POST">
                                            @csrf
                                            <input type='hidden' name='_method' value='DELETE'>
                                            <button class="btn btn-danger btn-fill">
                                                <i class="fa fa-trash"></i>
                                                Eliminar
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
