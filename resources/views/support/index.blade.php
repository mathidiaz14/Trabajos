@extends('layouts.app', ['active' => 'support'])

@section('content')
<div class="row">
    <div class="col-lg-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Equipos</li>
          </ol>
        </nav>
        <div class="card">
            <div class="header">
                <div class="row">
                    <div class="col-xs-6">
                        <h4 class="title">Equipos</h4>
                        <p class="category">Equipos registrados</p>
                    </div>
                    <div class="col-xs-6 text-right">
                        <a href="{{url('support/create')}}" class="btn btn-success btn-fill">
                            <i class="fa fa-plus"></i>
                            Agregar
                        </a>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="table table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                              <th scope="col">Numero</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Descripción</th>
                              <th scope="col">Empresa</th>
                              <th scope="col">Editar</th>
                              <th scope="col">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(Auth::user()->supports as $support)
                                <tr>
                                    <td># {{$support->number}}</td>
                                    <td>{{$support->type}}</td>
                                    <td>{{$support->description}}</td>
                                    <td><a href="{{url('business', $support->business_id)}}">{{$support->business->name}}</a></td>
                                    <td>
                                        <a href="{{route('support.edit', $support->id)}}" class="btn btn-primary btn-fill">
                                            <i class="fa fa-edit"></i>
                                            Editar
                                        </a>
                                    </td>
                                    <td>
                                        <form action="{{ url('support', $support->id)}}" method="POST">
                                            @csrf
                                            <input type='hidden' name='_method' value='DELETE'>
                                            <button class="btn btn-danger btn-fill">
                                                <i class="fa fa-trash"></i>
                                                Eliminar
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
