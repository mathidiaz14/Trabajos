<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Work;
use Auth;
use Carbon\Carbon;

class WorkController extends Controller
{
    private $path = "work.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->path."index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path."create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $work               = new Work();
        
        if ($request->date == null) 
            $date = Carbon::now();
        else
            $date = $request->date;

        $work->date         = $date;
        $work->description  = $request->description;
        $work->cost         = $request->cost;
        $work->gain         = $request->gain;
        $work->saved        = $request->saved;
        $work->expires      = $request->expires;
        $work->status       = $request->status;
        $work->business_id  = $request->business_id;
        $work->user_id      = Auth::user()->id;

        $work->save();

        return redirect('work');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $work = Work::find($id);

        return view($this->path."edit", compact('work'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $work               = Work::find($id);
        
        if ($request->date == null) 
            $date = Carbon::now();
        else
            $date = $request->date;

        $work->date         = $date;
        $work->description  = $request->description;
        $work->cost         = $request->cost;
        $work->gain         = $request->gain;
        $work->saved        = $request->saved;
        $work->expires      = $request->expires;
        $work->status       = $request->status;
        $work->business_id  = $request->business_id;
        $work->save();

        return redirect('work');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $work = Work::find($id);
        $work->delete();

        return back();
    }
}
