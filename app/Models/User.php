<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function categories()
    {
        return $this->hasMany('App\Models\Category');
    }

    public function businesses()
    {
        return $this->hasMany('App\Models\Business');
    }

    public function supports()
    {
        return $this->hasMany('App\Models\Support');
    }

    public function works()
    {
        return $this->hasMany('App\Models\Work');
    }

    public function passwords()
    {
        return $this->hasMany('App\Models\Password');
    }
}
