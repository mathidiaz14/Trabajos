<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Password extends Model
{
    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }
}
