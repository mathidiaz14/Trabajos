<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    public function notes()
    {
    	return $this->hasMany('App\Models\Note')->orderBy('created_at', 'desc');
    }

    public function category()
    {
    	return $this->belongsTo('App\Models\Category');
    }

    public function conversations()
    {
    	return $this->hasMany('App\Models\Conversation')->orderBy('created_at', 'desc');
    }
}
