<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
   	public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    public function works()
    {
    	return $this->hasMany('App\Models\Work');
    }

    public function passwords()
    {
    	return $this->hasMany('App\Models\Password');
    }
}
